from django.shortcuts import render, redirect
from django.contrib.auth.models import User, auth 
from .models import hss
from django.http import HttpResponse 
from django.template import loader
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User 
from django.db import IntegrityError 
from django.contrib import messages


# def hs(request):  
#     template = loader.get_template('hs.html')
#     return HttpResponse(template.render())

def horscope(request):  
    template = loader.get_template('horscope.html')
    return render(request, 'horscope.html')

def scope(request):
    stud = hss.objects.all()
    return render(request, 'hs.html', {'stu' : stud})
  

def intro(request):  
    template = loader.get_template('intro.html')
    return render(request, 'intro.html')


def Register(request):
  if request.method=="POST":
    if request.POST.get('password1')==request.POST.get('password2'):
      try:
        saveuser=User.objects.create_user(request.POST.get('username'),password=request.POST.get('password1'))
        saveuser.save()
        return render(request,'signup.html',{'form':UserCreationForm(),'info':'The user'+' '+request.POST.get('username')+' registration successful!'})
      except IntegrityError:
        return render(request,'signup.html',{'form':UserCreationForm(),'error':'The user'+' '+request.POST.get('username')+' user already exist!'})
    else:
      return render(request,'signup.html',{'form':UserCreationForm(),'error':'Password should be same!'})  
  else:
    return render(request,'signup.html',{'form':UserCreationForm})

def UserLogin(request):
  if request.method=="POST":
    loginsuccess=authenticate(request,username=request.POST.get('username'),password=request.POST.get('password'))
    if loginsuccess is None:
      return render(request,'signin.html',{'form':AuthenticationForm(),'error':'Invalid Username or Password...!'})
    else:
      login(request,loginsuccess)
      return redirect('horscope')
  else:
    return render(request,'signin.html',{'form':AuthenticationForm()})
  