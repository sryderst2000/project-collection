import React, { useEffect, useState } from "react";
import { Table, Button } from "react-bootstrap";
import { storage } from "./firebase";
import CheytamDataService from "./helper/cheytam.services";
import Navbar from './Navbar'
import { ref } from "firebase/storage";


const Home = ({ getCheytamId }) => {
  const [song, setSong] = useState([]);
  useEffect(() => {
    getSong();
  }, []);

  const getSong = async () => {
    const data = await CheytamDataService.getAllSong();
    console.log(data.docs);
    setSong(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
  };

  const deleteHandler = async (id) => {
    await CheytamDataService.deleteSong(id);
    getSong();
  };
  const audioList=ref(storage,'Upload/')

  

  return (
    <>
    <Navbar/>
      
      <h4 style={{marginTop:'6%',marginLeft:'40%' }}>Uploaded Videos</h4>
      <Table striped bordered hover size="sm" style={{ padding:35,backgroundColor:'#cccc'}}>
        <thead >
          <tr>
            <th >#</th>
           
            <th>VideoTitle</th>
          
            <th>Descriptions</th>
           
          </tr>
        </thead>
        <tbody>
          {song.map((doc, index,url) => {
            return (
              <tr key={doc.id} >
                <td>{index + 1}</td>
                <td>{doc.title}</td>
               
                <td>{doc.lyric}</td>
                <td>
             
                    <video width="200" height="100" controls >
                      <source src={doc.audioUrl} type="video/mp4"/>
                    </video>
                </td>
                <td>
                 
                  <Button
                    variant="danger"
                    className="delete"
                    onClick={(e) => deleteHandler(doc.id)}
                  >
                    Delete
                  </Button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>


    </>
  );
};

export default Home;