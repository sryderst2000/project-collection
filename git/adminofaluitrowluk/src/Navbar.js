

import { Navbar,Nav } from 'react-bootstrap'
const Navbarmenu = () => {
  return (
    <>
    
<div className="row">
    <div className="col-md-12">
            <Navbar  expand="lg"  style={{backgroundColor:'#27C1FA',color:'white',
                position:'fixed',
                display:'flex',
                overflow:'visible',
                zIndex:1,
                width:'100%',
                margin:'auto',
                top:0,}}>
                <Navbar.Brand href="/Home" style={{marginLeft:30,color:'white',fontSize:20,fontFamily:'serif'}}>View Uploads</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto" style={{padding:10,marginLeft:10,}}>
                  
                    <Nav.Link style={{marginLeft:30,color:'white',fontSize:20,fontFamily:'serif'}} href="/upload">Upload Video</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
    </div>
</div>
    </>
  );
};
  
export default Navbarmenu;





