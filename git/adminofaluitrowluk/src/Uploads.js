
import {useState,useEffect} from 'react'

import './forms.css'
import Navbar from './Navbar'
import 'bootstrap/dist/css/bootstrap.css';
import Form from 'react-bootstrap/Form';
import {Card,Button} from 'react-bootstrap'; 

import{Timestamp,addDoc,collection} from 'firebase/firestore'
import{uploadBytes,getDownloadURL,ref}from 'firebase/storage'
import {v4} from 'uuid'
import { db } from './firebase';
import  {storage}  from './firebase'; 

import CheytamDataService from './helper/cheytam.services';




const Uploads=({id,getCheytamId})=>{
    const[title, setTitle]=useState('');
    const [lyric, setLyric]=useState('');
    const[audio,setAudio]=useState(null);
    const [audioList,setAudioList]=useState([])
    const [message, setMessage] = useState({ error: false, msg: "" });

  
    const addBoedra=()=>{
      if(audio==null)return;
      const audioRef=ref(storage,`Upload/${audio.name + v4()}`);
      uploadBytes(audioRef,audio).then((snapshot)=>{
      alert('Successful uploaded');
          getDownloadURL(snapshot.ref).then((url)=>{
              setAudioList((prev)=>[...prev,url]);

          const collectionRef=collection(db,`Upload/`);
              addDoc(collectionRef,{
                title:title,
                lyric:lyric,
                audioUrl:url,
                createdAt:Timestamp.now().toDate()
            });
             

          })
          
      })
  }
  const editHandler = async () => {
    setMessage("");
    try {
      const docSnap = await CheytamDataService.getCheytamId(id);
      console.log("the record is :", docSnap.data());
      setTitle(docSnap.data().title);
      setLyric(docSnap.data().lyric);
      
      setAudio(docSnap.data().audio);
    } catch (err) {
      setMessage({ error: true, msg: err.message });
    }
  };

  useEffect(() => {
    console.log("The id here is : ", id);
    if (id !== undefined && id !== "") {
      editHandler();
    }
  }, [id]);

  return(
    <>
     <Navbar sticky='top'/>   
     <div style={{display: 'block', 
                  width: 650, 
                  padding: 25,
                  height:'100%',
                  marginLeft:370,
                  borderWidth:1,
                  borderColor:'#cccc',
                  backgroundColor:'#cccc',
                  marginTop:75,
                  borderRadius:6}}>
      <Form>
      <Form.Group>
      <Card.Title>Title</Card.Title>
          <Form.Control type="text" 
                        placeholder="title" 
                        required
                        onChange={(e)=>{setTitle(e.target.value)}}/>
        </Form.Group>
        <Form.Group>
        <Card.Title>Descriptions</Card.Title>   
        <textarea
        placeholder='Video Description...'
        className="form-control"
        required
        id="exampleFormControlTextarea1"
        rows="10"
        onChange={(e)=>{setLyric(e.target.value)}}/>
        </Form.Group>
    
        <Form.Group>
           
    
          <Form.Control id='files' type="file" 
           onChange={(e)=>{setAudio(e.target.files[0])}}
          required

          />
     
        </Form.Group>
        <div style={{marginTop:10}}>
        <Button variant="primary" onClick={addBoedra}>
           submit
        </Button>
        </div>
       
      </Form>
    </div>
    </> 

    
  )
}

export default Uploads;
