
import { db } from "../firebase";

import { 
    collection, 
    getDocs, 
    getDoc,
    addDoc,
    updateDoc,
    deleteDoc,
     doc,
     } from "firebase/firestore";
    
const collectionRef = collection(db, "Upload");
class CheytamDataService {
   
    addSong = (newSong) => {
        return addDoc(collectionRef, newSong);
    };

    updateSong = (id, updateSong) => {
        const Doc =doc(db, "Upload", id);
        return updateDoc(Doc, updateSong);
    };

    deleteSong = (id) => {
        const Doc = doc(db, "Upload", id);
        return deleteDoc(Doc);
    };

    getAllSong = () => {
        return getDocs(collectionRef);
    };

    getSong = (id) => {
        const  Doc = doc(db, "Upload" ,id);
        return getDoc(Doc);
    };
}
export default new CheytamDataService();