import React,{useState} from "react";
import Form from 'react-bootstrap/Form';
import {Card,Button} from 'react-bootstrap'; 
import { db } from "./firebase";
import { doc } from "firebase/firestore";

function FbEdit({getId,setEditbox,getBookId}){
    const[title, setTitle]=useState('');
    const[artist,setArtist]=useState('');
    const [lyric, setLyric]=useState('');
    const[audio,setAudio]=useState(null);
    const [audioList,setAudioList]=useState([])

    function editDoc(updateBook){
    db.doc(updateBook.id)
    .update(updateBook)
    .catch((err)=>{
        alert(err)
        console.log(err)
    })

    }


    return(
       <>
            <div style={{display: 'block', 
                  width: 650, 
                  padding: 25,
                  height:'100%',
                  marginLeft:370,
                  borderWidth:1,
                  borderColor:'#cccc',
                  backgroundColor:'#cccc',
                  marginTop:10,
                  borderRadius:6}}>
      <Form>
      <Form.Group>
      <Card.Title>Title</Card.Title>
          <Form.Control type="text" 
                        placeholder="title" 
                        required
                        onChange={(e)=>{setTitle(e.target.value)}}/>
        </Form.Group>
        <Form.Group>
        <Card.Title>Lyric</Card.Title>   
        <textarea
        placeholder='Write lyric...'
        className="form-control"
        required
        id="exampleFormControlTextarea1"
        rows="10"
        onChange={(e)=>{setLyric(e.target.value)}}/>
        </Form.Group>
        <Form.Group>
      <Card.Title>Artist</Card.Title>
          <Form.Control type="text" 
                        
                        onChange={(e)=>{setArtist(e.target.value)}}/>
        </Form.Group>
        <Form.Group>
           
    
          <Form.Control id='files' type="file" 
           onChange={(e)=>{setAudio(e.target.files[0])}}
          required

          />
     
        </Form.Group>
        <div style={{marginTop:10}}>
        <Button variant="primary"
        onClick={()=>{
            getBookId(doc.id)
            setEditbox(false)
        }} >
           Update
        </Button>
        </div>
       
      </Form>
    </div>
       </>
    )
}

export default FbEdit;