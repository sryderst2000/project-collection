import './App.css';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import Home from './Home';
import Login from './Login'
import {useState, useEffect} from 'react'
import {AuthProvider} from './AuthContext'
import {auth} from './firebase'
import {onAuthStateChanged} from 'firebase/auth'

import Uploads from './Uploads';


function App() {

  const [currentUser, setCurrentUser] = useState(null)


  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      setCurrentUser(user)
    })
  }, [])

  return (
    <Router>
    
      <AuthProvider>
  
        <Routes>
        <Route exact path="/" element={<Login/>} />
        <Route path="/home" element={<Home/>} />
      
        <Route path="/upload" element={<Uploads/>} />

        
        </Routes>  
      </AuthProvider>
  </Router>
  );
}

export default App;
