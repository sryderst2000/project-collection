import React,{useState,useEffect} from 'react'
import { View, Image, Text, SafeAreaView, ScrollView, StyleSheet } from 'react-native'
import firebase from '../Firebase/config';
import { Video } from 'expo-av'

const Videos = () => {
    const [imageTab, setImageTab] = useState([]);
  
    useEffect(() => {
      firebase.storage()
        .ref()
        .listAll()
        .then(function(result) {
            result.items.forEach(function(imageRef) {
                imageRef.getDownloadURL().then(function(url) {
                    imageTab.push(url);
                    setImageTab(imageTab);
                }).catch(function(error) {
                    // Handle any errors
                });
            });
        })
        .catch((e) => console.log('Errors while downloading => ', e));
    }, []);
  
    return (
        <View  style={{justifyContent: 'center'}}>
            <ScrollView>
                {imageTab.map(i => (<Video style={{height: 200, width: 300}} source={{uri: i}}  
                    useNativeControls
                    resizeMode="contain"
                    key={i.toString()}
                    isLooping/>
                    
                    ))}
            </ScrollView>
      </View>);
  }
  
  export default Videos;

  const styles = StyleSheet.create({

  })