import React,{useState,useEffect} from 'react'
import { StyleSheet, Text, View, FlatList, ActivityIndicator, ScrollView } from 'react-native'
import firebase from '../Firebase/config';
import { Video } from 'expo-av'

export default class VideoList extends React.Component {
  constructor() {
    super();
    this.firestoreRef = firebase.firestore().collection('Upload');
    this.state = {
      isLoading: true,
      userArr: [],
    };
  }
  componentDidMount() {
    this.unsubscribe = this.firestoreRef.onSnapshot(this.getCollection);
  }
  componentWillUnmount(){
    this.unsubscribe();
  }
  getCollection = (querySnapshot) => {
    const userArr = [];
    querySnapshot.forEach((res) => {
      const { title, lyric,audioUrl } = res.data();
      userArr.push({
        key: res.id,
        res,
        title,
        lyric,
        audioUrl,
      });
    });
    this.setState({
      userArr,
      isLoading: false,
    });
  }
  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }
    const renderInfo = ({ item,id }) => {
      return (
          <View  style={styles.videoContainer} key={id}>
            <Video
              style={styles.video}
              source={{uri:item.audioUrl}}
              useNativeControls
              resizeMode="contain"
            />
            <View style={styles.postDetailContainer}>
              <Text style={{fontWeight:'bold', color: 'white'}}>Title: {item.title}</Text>
              <Text style={{color: 'white'}}>Descriptions: {item.lyric}</Text>
            </View>
          </View>
      )
  }
return (
  <View style={styles.safeView}>
    <View style={styles.flatlist}>
      {this.state.userArr.length > 0 && (
              <FlatList
                  data={this.state.userArr}
                  renderItem={renderInfo}
                  keyExtractor={(info) => info.id}
              />
      )}
    </View>
  </View>
)
}
}

const styles = StyleSheet.create({
  safeView:{
    height: '100%',
    width: '100%',
    padding: 10
  },
  video:{
    width: '100%',
    height: 170,
    marginBottom:15,
    marginTop:15
  },
  videoContainer:{
    width:'100%',
    marginBottom:10,
    backgroundColor:'white'
  },
  flatlist:{
    flex:1,
  },
  postDetailContainer:{
    paddingHorizontal:20,
    paddingVertical:10,
    backgroundColor:'#3498db',
  },
})

