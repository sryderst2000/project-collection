import firebase from "firebase/compat"
import 'firebase/compat/storage'


const firebaseConfig = {
  apiKey: "AIzaSyCjSrhh4rbVSrr5XmxU_hqBgrnuYrgUP8Y",
  authDomain: "aluitrowlu-8b7d3.firebaseapp.com",
  databaseURL: "https://aluitrowlu-8b7d3-default-rtdb.firebaseio.com",
  projectId: "aluitrowlu-8b7d3",
  storageBucket: "aluitrowlu-8b7d3.appspot.com",
  messagingSenderId: "440222374537",
  appId: "1:440222374537:web:c9821aa01912ea6cfc4458"
};
  
  firebase.initializeApp(firebaseConfig);

  const storage =  firebase.storage();

  export {storage, firebase as default}
