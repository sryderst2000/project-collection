import { Dimensions } from "react-native";
const {width, height} = Dimensions.get('window');

export const COLORS = {
    // primary: "#252c4a",
    primary: "#a7c4db",
    secondary: '#1E90FF',
    accent: '#3498db',
    
    success: '#00C851',
    error: '#ff4444',

    black: "#252c4a",
    white: "#FFFFFF",
    background: "#252C4A",
    tertiry: '#252c4a'
}


export const SIZES = {
    base: 10,
    width,
    height
}