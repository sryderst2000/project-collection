import { SafeAreaView, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Videos from '../components/Videos'

const Contain = () => {
  return (
    <SafeAreaView style={{marginTop: 40}}>
    <View style={{backgroundColor:'lightblue', height: '100%', alignItems: 'center'}}>
      <Text style={{fontWeight: 'bold', fontSize: 24}}>VIDEOS</Text>
      <Videos/>
    </View>
    </SafeAreaView>
  )
}

export default Contain

const styles = StyleSheet.create({})