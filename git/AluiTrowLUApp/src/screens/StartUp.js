import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

const StartUP = ({navigation}) => {
    setTimeout(()=>{
        navigation.navigate('HomeScreen')
    }, 3000)
  return (
      <Image
        style={styles.StartupContainer}
        source ={require("../assets/logo.png") }
        resizeMode= "contain"/>
  );
};
const styles = StyleSheet.create({
  StartupContainer: {
  //   flex:1,
   
    marginTop:'50%'
    
  },
});

export default StartUP;