import { Image } from 'react-native'
import { Audio } from 'expo-av';
import { View, Text } from 'react-native'
import { COLORS, SIZES } from '../constants/theme';

export default data = [
    {
        question: <View style={{justifyContent: 'center', alignItems: 'center', width: '100%'}}>
                    <Text style={{color: COLORS.white, fontSize: 25}}>ཚོད་བསྲི་་འདི་ངོས་འཛིན་འབད།</Text>
                    <Image style={{width: 300,height:280}} source={require('../../assets/yellow.png')}/>
                  </View>,
        options: ["ཀ་  འདབ་མ་ཀོ་པི་ཨིན།","ཁ་  ལ་ཕུག་ཨིན།","ག་  ཨེ་མ་ཨིན།","ང་   དོ་ལོམ་ཨིན།"],
        correct_option: "ཀ་  འདབ་མ་ཀོ་པི་ཨིན།"
    },
    {
        question: <View style={{justifyContent: 'center', alignItems: 'center', width: '100%'}}>
                    <Text style={{color: COLORS.white, fontSize: 25}}>ཚོད་བསྲི་་འདི་ངོས་འཛིན་འབད།</Text>
                    <Image style={{width: 350,height:280}} source={require('../../assets/red.png')}/>
                  </View>,
        options: ["ཀ་  སྒོགཔ་ཨིན།","ཁ་  ལ་ཕུག་དམརཔོ་ཨིན།","ག་  ཀ་ཀུ་རུ་ཨིན།", "ང་   ཨེ་མ་ཨིན།"],
        correct_option: "ང་   ཨེ་མ་ཨིན།"
    },
    {
        question:<View style={{justifyContent: 'center', alignItems: 'center', width: '100%'}}>
                    <Text style={{color: COLORS.white, fontSize: 25}}>ཚོད་བསྲི་་འདི་ངོས་འཛིན་འབད།</Text>
                    <Image style={{width: 350,height: 280}} source={require('../../assets/green.png')}/>
                </View>,
        options: ["ཀ་  ལ་ཕུག་ཨིན།","ཁ་  དོ་ལོམ་ཨིན།","ག་  ལ་ཕུག་དམརཔོ་ཨིན།", "ང་  ཀ་ཀུ་རུ་ཨིན།"],
        correct_option: "ག་  ལ་ཕུག་དམརཔོ་ཨིན།"
    },
    {
        question: <View style={{justifyContent: 'center', alignItems: 'center', width: '100%'}}>
                    <Text style={{color: COLORS.white, fontSize: 25}}>ཚོད་བསྲི་་འདི་ངོས་འཛིན་འབད།</Text>
                    <Image style={{width: 350,height: 300}} source={require('../../assets/orange.png')}/>
                  </View>,
        options: ["ཀ་  སྒོགཔ་ཨིན།","ཁ་  ལ་ཕུག་དམརཔོ་ཨིན།","ག་  ཀེ་བ་ཨིན།", "ང་  ཧོན་ཚོད་ཨིན།"],
        correct_option: "ག་  ཀེ་བ་ཨིན།"
    },
    {
        question: <View style={{justifyContent: 'center', alignItems: 'center', width: '100%'}}>
                    <Text style={{color: COLORS.white, fontSize: 25}}>ཚོད་བསྲི་་འདི་ངོས་འཛིན་འབད།</Text>
                    <Image style={{width: 350,height: 350}} source={require('../../assets/black.png')}/>
                  </View>,
        options: ["ཀ་  ལ་ཕུག་དམརཔོ་ཨིན།","ཁ་  ་ཨིན།","ག་  ཀེ་བ་ཨིན།","ང་  ཧོན་ཚོད་ཨིན།"],
        correct_option: "ང་  སྟག་ཨིན།"
    }
]
