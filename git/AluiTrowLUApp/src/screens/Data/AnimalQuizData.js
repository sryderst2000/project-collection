import { Image } from 'react-native'
import { View, Text } from 'react-native'

export default data = [
    {
        question: <View>
                    <Image style={{width:350,height:230}} source={require('../../assets/deer.png')}/>
                  </View>,
        options: ["ང་ཀ་ཤ་ཨིན།","ང་བྱི་ལི་ཨིན།","ང་རོ་ཁྱི་ཨིན།","ང་རྟ་ཨིན།"],
        correct_option: "ང་ཀ་ཤ་ཨིན།"
    },
    {
        question: <View>
                    <Image style={{width:350,height:230}} source={require('../../assets/deer.png')}/>
                </View>,
        options: ["སྒོ་ནོར་སམས་ཅན་ཨིན།","རི་ཁའི་སམས་ཅན་ཨིན།"],
        correct_option: "རི་ཁའི་སམས་ཅན་ཨིན།"
    },
    {
        question: "སམས་ཅན་ཀ་ཤ་འདི་སྡོ་སར་ག་ཏེ་སྨོ?",
        options: ["ནོར་ཁྱིམ་ནང་ཨིན","རྟ་ཁྱིམ་ནང་ཨིན","གཡུས་ཁར་ཨིན།","ནགས་ཚལ་ནང་ཨིན"],
        correct_option: "ནགས་ཚལ་ནང་ཨིན"
    },
    {
        question: "སམས་ཅན་བ་གི་ག་ཅིག་བྱིན་ནི་ཡོད ?",
        options: ["ཨོམ།","བལ།"," སྒོང་རྡོག།","ཏི་རུ།"],
        correct_option: "ཨོམ།"
    },
    {
        question:  <Image style={{width:350,height:200}} source={require('../../assets/cat.jpeg')}/>,
        options: ["བྱི་ལི་༡་འདུག།","བྱི་ལི་༦་འདུག།","བྱི་ལི་༤་འདུག།","བྱི་ལི་༥་འདུག།"],
        correct_option: "བྱི་ལི་༥་འདུག།"
    }
]
