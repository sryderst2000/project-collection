import React,{useState} from 'react';
import {SafeAreaView,View,ScrollView, StyleSheet, Image,Alert, TouchableOpacity,  Dimensions, useWindowDimensions, KeyboardAvoidingView,} from 'react-native';
import Colors from "../config/Colors"
import { Button, Text, TextInput} from 'react-native-paper';
import VideoList from '../components/VideoList'
import { COLORS } from './constants/theme';
import SearchBar from '../components/SearchBar';
import Header from '../components/Header';
import { AntDesign } from "@expo/vector-icons";

const HomeScreen = ({navigation}) => {

  const {width, height} = useWindowDimensions();

  const marginTopDistance = height < 380 ? 30: 100;
  return (
  <SafeAreaView style={styles.Container} > 
    <View  style={styles.subView} >
      <View style={styles.RhymeView}>
        <View style={{ width: '78%', justifyContent: 'flex-start', flexDirection: 'row'}}>
          <Image style={{width:50,height:50, marginTop:5}} source={require('../assets/logo.png')}/>
          <Header>ཨ་ལོའི་སྤྲོ་གླུ།</Header>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity onPress={() => navigation.navigate('AboutScreen')}>
            <AntDesign name="infocirlce" size={28} color="white" style={{marginRight: 15}} />
          </TouchableOpacity>
          <TouchableOpacity 
            onPress={() => 
              Alert.alert(
                'འཐོན་སྒོ།',
                'འཐོན་འགྱོ་ནི་ཨིན?',
                [
                  {text: 'ཨིན།', onPress: () => alert('Successfully logut.')},
                  {text: 'མེན།', onPress: () => console.log('Canceled'), style: 'cancel'},
                ],
                { 
                  cancelable: true 
                }
            )}
          >
            <AntDesign name="logout" size={28} color="white" />
          </TouchableOpacity>
        </View>
      </View>
      <View style={{justifyContent: 'center', flexDirection: 'row', alignItems: 'center', marginTop:10}}>
        <SearchBar />
        <Button style={styles.button}
              onPress={() => navigation.navigate('QuizHomeScreen')}
            >
              <Text style={styles.buttonText}>རིག་བསྡུར།</Text>
            </Button>
      </View>
      <View style={{ height:'85%'}}>
        <View style={[ { height: '100%', minHeight:'100%'} ]}>
          <VideoList/>
        </View>
      </View>
    </View>
  </SafeAreaView>
  );
};

const deviceHeight = Dimensions.get('window').height; 

const styles = StyleSheet.create({
  button:{
    backgroundColor:'#252c4a',
    width:90,
    height: 45,
    borderRadius:10,  
    alignItems: 'center'
    
  },
  buttonText:{
    marginTop:10,
    color:Colors.white,
    fontSize: 14

  },
  Container:{
    flex:1,
    backgroundColor:Colors.white,
    marginTop: 40

  },
  VideoContainer:{
    width:320,
    height:150,
    backgroundColor:'#252c4a',
  },
  RhymeView:{
    backgroundColor:'#252c4a',
    height:60,
    width:"100%",
    color: 'white',
    flexDirection: 'row'
  
  },
  row:{
    flexDirection:"row",
    justifyContent:"space-around",
    padding:10,
  },
  subView:{
    backgroundColor:COLORS.primary,
    justifyContent: 'center',
    flex:1,

  },
 
  text:{
    fontSize:20,
    padding:10,
    top:10,
    left:22,
    color: 'white'
  },

  textInput:{
    position:"absolute",
    width:"100%",
    borderWidth:1,
    top:40,
    borderRadius:4,
    height:50

  },
 
})

export default HomeScreen;