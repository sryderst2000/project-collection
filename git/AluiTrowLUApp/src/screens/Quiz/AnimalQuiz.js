import React, { useState } from 'react'
import { View, Text, SafeAreaView, StatusBar, Image,Dimensions, useWindowDimensions, TouchableOpacity, Modal, Animated, ScrollView } from 'react-native'
import { COLORS, SIZES } from '../constants/theme';
import data from '../Data/AnimalQuizData';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Button from '../../components/Button';

const AnimalQuiz = ({navigation}) => {
    const {width, height} = useWindowDimensions();

    const allQuestions = data;
    const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0)
    const [currentOptionSelected, setCurrentOptionSelected] = useState(null);
    const [correctOption, setCorrectOption] = useState(null);
    const [isOptionsDisabled, setIsOptionsDisabled] = useState(false);
    const [score, setScore] = useState(0)
    const [showNextButton, setShowNextButton] = useState(false)
    const [showScoreModal, setShowScoreModal] = useState(false)

    const validateAnswer = (selectedOption) => {
        let correct_option = allQuestions[currentQuestionIndex]['correct_option'];
        setCurrentOptionSelected(selectedOption);
        setCorrectOption(correct_option);
        setIsOptionsDisabled(true);
        if(selectedOption==correct_option){
            // Set Score
            setScore(score+1)
        }
        // Show Next Button
        setShowNextButton(true)
    }
    const handleNext = () => {
        if(currentQuestionIndex== allQuestions.length-1){
            // Last Question
            // Show Score Modal
            setShowScoreModal(true)
        }else{
            setCurrentQuestionIndex(currentQuestionIndex+1);
            setCurrentOptionSelected(null);
            setCorrectOption(null);
            setIsOptionsDisabled(false);
            setShowNextButton(false);
        }
        Animated.timing(progress, {
            toValue: currentQuestionIndex+1,
            duration: 1000,
            useNativeDriver: false
        }).start();
    }
    const restartQuiz = () => {
        setShowScoreModal(false);

        setCurrentQuestionIndex(0);
        setScore(0);

        setCurrentOptionSelected(null);
        setCorrectOption(null);
        setIsOptionsDisabled(false);
        setShowNextButton(false);
        Animated.timing(progress, {
            toValue: 0,
            duration: 1000,
            useNativeDriver: false
        }).start();
    }

    const renderQuestion = () => {
        return (
            <View style={{
                marginVertical: 0
            }}>
                {/* Question Counter */}
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'flex-end'
                }}>
                    <Text style={{color: COLORS.white, fontSize: 20, opacity: 0.6, marginRight: 2}}>{currentQuestionIndex+1}</Text>
                    <Text style={{color: COLORS.white, fontSize: 18, opacity: 0.6}}>/ {allQuestions.length}</Text>
                </View>

                {/* Question */}
                <Text style={{
                    color: COLORS.white,
                    fontSize: 25
                }}>{allQuestions[currentQuestionIndex]?.question}</Text>
            </View>
        )
    }
    const marginTopDistance = height < 380 ? 30: 100;

    const renderOptions = () => {
        return (
            <View>
                {
                allQuestions[currentQuestionIndex]?.options.map(option => (
                    <TouchableOpacity 
                    onPress={()=> validateAnswer(option)}
                    disabled={isOptionsDisabled}
                    key={option}
                    style={{
                        borderWidth: 3, 
                        borderColor: option==correctOption 
                        ? COLORS.success
                        : option==currentOptionSelected 
                        ? COLORS.error 
                        : COLORS.secondary+'40',
                        backgroundColor: option==correctOption 
                        ? COLORS.success +'20'
                        : option==currentOptionSelected 
                        ? COLORS.error +'20'
                        : COLORS.tertiry,
                        height: 60, borderRadius: 20,
                        flexDirection: 'row',
                        alignItems: 'center', justifyContent: 'space-between',
                        paddingHorizontal: 20,
                        marginVertical: 10
                    }}
                    >
                    <Text style={{fontSize: 20, color: COLORS.white}}>{option}</Text>
                    {
                        option==correctOption ? (
                            <View style={{
                                width: 30, height: 30, borderRadius: 30/2,
                                backgroundColor: COLORS.success,
                                justifyContent: 'center', alignItems: 'center'
                            }}>
                                <MaterialCommunityIcons name="check" style={{
                                    color: COLORS.white,
                                    fontSize: 20
                                }} />
                            </View>
                        ): option == currentOptionSelected ? (
                            <View style={{
                                width: 30, height: 30, borderRadius: 30/2,
                                backgroundColor: COLORS.error,
                                justifyContent: 'center', alignItems: 'center'
                            }}>
                                <MaterialCommunityIcons name="close" style={{
                                    color: COLORS.white,
                                    fontSize: 20
                                }} />
                            </View>
                        ) : null
                    }

                    </TouchableOpacity>
                    ))
                }
            </View>
        )
    }
    const renderNextButton = () => {
        if(showNextButton){
            return (
                <TouchableOpacity
                onPress={handleNext}
                style={{
                    marginTop: 0, width: '100%', backgroundColor: COLORS.accent, padding: 20, borderRadius: 30
                }}>
                    <Text style={{fontSize: 20, color: COLORS.white, textAlign: 'center'}}>ཤུལ་མ།</Text>
                </TouchableOpacity>
            )
        }else{
            return null
        }
    }

    const [progress, setProgress] = useState(new Animated.Value(0));
    const progressAnim = progress.interpolate({
        inputRange: [0, allQuestions.length],
        outputRange: ['0%','100%']
    })
    const renderProgressBar = () => {
        return (
            <View style={{
                width: '100%',
                height: 20,
                borderRadius: 20,
                backgroundColor: '#252c4a',

            }}>
                <Animated.View style={[{
                    height: 20,
                    borderRadius: 20,
                    backgroundColor: COLORS.accent
                },{
                    width: progressAnim
                }]}>

                </Animated.View>

            </View>
        )
    }
    const deviceHeight = Dimensions.get('window').height; 
    return (
       <SafeAreaView>
           
                <View style={{
                    height: '100%',
                    paddingVertical: 20,
                    paddingHorizontal: 16,
                    backgroundColor: COLORS.primary,
                    position:'relative',
                    marginTop: 40,
                    minHeight: '100%'
                }}>
                    <ScrollView>

                        {/* ProgressBar */}
                        { renderProgressBar() }

                        {/* Question */}
                        {renderQuestion()}

                        {/* Options */}
                        {renderOptions()}

                        {/* Next Button */}
                        {renderNextButton()}

                        {/* Score Modal */}
                        <Modal
                        animationType="slide"
                        transparent={true}
                        visible={showScoreModal}
                        >

                            <View style={{
                                flex: 1,
                                backgroundColor: COLORS.primary,
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}>
                                <View style={{
                                    backgroundColor: COLORS.white,
                                    width: '90%',
                                    borderRadius: 20,
                                    padding: 20,
                                    alignItems: 'center'
                                }}>
                                    <Text style={{fontSize: 30, fontWeight: 'bold'}}>{ score> (allQuestions.length/2) ? 'བཀྲ་ཤི་བདེ་ལེགས།' : 'གོངམ་མ་འཁྲིལ་ལགས།' }</Text>

                                    <View style={{
                                        flexDirection: 'row',
                                        justifyContent: 'flex-start',
                                        alignItems: 'center',
                                        marginVertical: 10
                                    }}>
                                        <Text style={{
                                            fontSize: 30,
                                            color: score> (allQuestions.length/2) ? COLORS.success : COLORS.error
                                        }}>{score}</Text>
                                            <Text style={{
                                                fontSize: 20, color: COLORS.black
                                            }}>/ { allQuestions.length }</Text>
                                    </View>
                                    {/* Retry Quiz button */}
                                    <TouchableOpacity
                                    onPress={restartQuiz}
                                    style={{
                                        backgroundColor: COLORS.tertiry,
                                        padding: 20, width: '100%', borderRadius: 20
                                    }}>
                                        <Text style={{
                                            textAlign: 'center', color: COLORS.white, fontSize: 20
                                        }}>མཇུག་བསྡུ་ཡི།</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Modal>
                        <View style={{marginBottom: 150}}>
                            <Button onPress={() => navigation.navigate('QuizHomeScreen')}>རྒྱབ་ཕྱོགས་ལུ།</Button>  
                        </View>
                </ScrollView>
                </View>
            
       </SafeAreaView>
    )
}

export default AnimalQuiz